FROM node:carbon
RUN mkdir /app
WORKDIR /app
COPY package.json /app
COPY yarn.lock /app
COPY src/ /app
COPY public/ /app
RUN npm i
