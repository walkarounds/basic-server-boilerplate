module.exports = {
  apiName: 'Tamplate API',
  port: 3000,
  database: 'mongodb://localhost',
  log: {
    level: 'info',
    path: `${__dirname}/../logs/basic-server.log`,
  },
  dbName: 'testDb',
};
