module.exports = {
  port: 8000,
  database: 'mongodb://52.58.76.170',
  log: {
    level: 'info',
    path: '/root/logs/open-id-api.log',
  },
};
