const mongoose = require('mongoose');
const moment = require('moment');

const { Schema } = mongoose;

const StatusSchema = new Schema({
  desc: {
    type: String,
    enum: ['sent', 'processed', 'click', 'group_unsubscribe', 'dropped', 'delivered', 'deferred', 'bounce', 'open', 'spam_report', 'unsubscribe', 'group_resubscribe'],
    default: 'sent'
  },
  timestamp: {
    type: Date,
    default: moment.utc()
  }

}, { _id: false });


const MailSchema = new Schema({
  msgId: {
    type: String,
    required: true,
    unique: true
  },
  statusCode: {
    type: Number
  },
  dispatchedAt: {
    type: Date
  },
  status: {
    type: [StatusSchema]
  },
  recipients: {
    type: [String],
    required: true
  },
  sender: {
    type: String,
    required: true
  }

}, { timestamps: { createdAt: 'insert_ts', updatedAt: 'update_ts' } });

module.exports = MailSchema;
