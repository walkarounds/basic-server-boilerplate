const config = require('config');
const Schemas = require('./schemas');
const connectMongoose = require('./connectMongoose');

module.exports = async function initializeDatabase(connector = connectMongoose) {
  const connection = await connector();

  const mainConnection = connection.useDb(config.get('dbName'), { useCache: true });
  mainConnection.model('Mail', Schemas.mailSchema);
};
