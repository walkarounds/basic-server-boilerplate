const config = require('config');
const mongoose = require('mongoose');
const { URL } = require('url');

module.exports = async function connect() {
  try {
    mongoose.set('useNewUrlParser', true); // Based on https://mongoosejs.com/docs/deprecations.html#summary
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    await mongoose.connect(
      config.get('database'),
      { keepAlive: true, keepAliveInitialDelay: 300000, connectTimeoutMS: 20000 },
    );
    const connectionData = new URL(config.get('database'));
    const userPass = connectionData.username && connectionData.password && `${connectionData.username}:****@`;
    global.log.info(`Connected to MongoDB ${connectionData.protocol}//${userPass || ''}${connectionData.host}`);

    return mongoose.connection;
  } catch (err) {
    global.log.error({ err }, 'Error while connecting to MongoDB');

    throw err;
  }
};
