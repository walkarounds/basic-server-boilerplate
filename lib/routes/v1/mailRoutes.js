const Router = require('koa-router');
const handlers = require('./handlers');

module.exports = function accountRoutes() {
  const router = new Router({ prefix: '/mail' });

  router
    .get('/:mailId', handlers.getMail);

  return router.routes();
};
