const getMail = require('./mail/getMail');
const scrapeCal = require('./scraper/cal');

module.exports = {
  getMail,
  scrapeCal,
};
