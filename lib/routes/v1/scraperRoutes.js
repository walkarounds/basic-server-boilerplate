const Router = require('koa-router');
const handlers = require('./handlers');

module.exports = function accountRoutes() {
  const router = new Router({ prefix: '/scrape' });

  router
    .get('/cal', handlers.scrapeCal);

  return router.routes();
};
