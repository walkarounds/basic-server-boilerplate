const Router = require('koa-router');

const mailRoutes = require('./mailRoutes');
const scraperRoutes = require('./scraperRoutes');

module.exports = () => {
  const router = new Router({ prefix: '/api/v1' });
  router.use(mailRoutes());
  router.use(scraperRoutes());

  return router.routes();
};
