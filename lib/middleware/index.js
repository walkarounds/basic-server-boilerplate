const errorHandlerMiddleware = require('./errorHandlerMiddleware');
const requestLogMiddleware = require('./requestLogMiddleware');

module.exports = {
  errorHandlerMiddleware,
  requestLogMiddleware,
};
