const _ = require('lodash');
const Hopper = require('in-the-hopper');

function statusToLevel(status) {
  if (status >= 500) {
    return 'error';
  } else if (status >= 400) {
    return 'warn';
  }

  return 'info';
}

function ignore(ctx) {
  return ctx.status === 200 &&
    (ctx.path === '/ping' ||
    ctx.path === '/health_check')
  }

module.exports = (logger, opts) => {
  function handler(entry) {
    const level = statusToLevel(entry.status);
    logger[level](entry);
  }

  const hopper = Hopper({ handler, ignore, ...opts });

  hopper.addField('err', ctx => ctx.state.err);
  hopper.addField('ip', ctx => ctx.ip);
  hopper.addField('host', ctx => ctx.host);
  hopper.addField('headers', ctx => _.omit(ctx.headers, 'user-agent', 'referer', 'referrer', 'authorization', 'host'));
  hopper.addField('userAgent', ctx => ctx.get('user-agent') || undefined);
  hopper.addField('referrer', ctx => ctx.get('referer') || ctx.get('referrer') || undefined);
  hopper.addField('query', ctx => (!_.isEmpty(ctx.query) ? ctx.query : undefined));
  hopper.addField('body', ctx => ((ctx.status >= 400 && !_.isEmpty(ctx.request.body)) ? ctx.request.body : undefined));
  hopper.addField('accessToken', ctx => {
    const token = ctx.get('authorization');
    if (token && token.indexOf('Bearer') === 0) {
      return token.slice(7);
    }

    return token || undefined;
  });
  hopper.addField('accountId', ctx => _.get(ctx.state, 'userinfo.sub'));
  hopper.addField('username', ctx => _.get(ctx.state, 'userinfo.username'));
  hopper.addField('companyId', ctx => _.get(ctx.state, 'userinfo.companyId'));
  hopper.addField('requestId', ctx => ctx.state.requestId);

  return hopper;
};
