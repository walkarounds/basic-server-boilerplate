const Promise = require('bluebird');
const moment = require('moment');

class MailModel {
  constructor(dbModel) {
    this._dbModel = dbModel;
  }

  fetchMailById(mailId) {
    return this._dbModel.findById(mailId).lean().exec();
  }

  addMail(data) {
    const mailData = {
      msgId: data.msgId,
      statusCode: data.statusCode,
      dispatchedAt: data.msgDate,
      recipients: data.params.to,
      sender: data.params.from,
      status: {}
    };
    return this._dbModel.create(mailData);
  }

  updateMailDetails(data) {
    return Promise.map(data, mailDetails => this._dbModel.findOneAndUpdate({ msgId: mailDetails.sg_message_id.split('.')[0] }, { $push: { status: { desc: mailDetails.event, timestamp: moment.utc(mailDetails.timestamp, 'X') } } }, { runValidators: true }).then(doc => doc).catch(err => err));
  }
}

module.exports = MailModel;
